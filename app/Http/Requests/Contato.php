<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class Contato extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required',
            'email' => 'required|email',
            'assunto' => 'required',
            'mensagem' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'nome.required' => 'Por favor, informe seu nome',
            'email.required' => 'Por favor, informe seu e-mail',
            'email.email' => 'Por favor, informe um e-mail válido',
            'assunto.required' => 'Por favor, informe o assunto',
            'mensagem.required' => 'Por favor, informe sua mensagem',
        ];
    }
}
