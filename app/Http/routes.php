<?php

get('/', function () {
    return view('home');
});

get('contato', function () {
    return view('contato');
});
get('servicos', function () {
    return view('servicos');
});

get('parceiros', function () {
    return view('parceiros');
});

get('faq', function () {
    return view('faq');
});

post('contato', function () {
    Mail::send('emails.contato', Request::all(), function ($mail) {
        $mail->to('atendimento@iasaseguros.com.br');
        $mail->subject('Contato via Site :: Página Contato');
    });

    session()->flash('success', 'Mensagem encaminhada com sucesso');

    return back();
});

post('servicos', function () {
    Mail::send('emails.servicos', Request::all(), function ($mail) {
        $mail->to('atendimento@iasaseguros.com.br');
        $mail->subject('Contato via Site :: Pagina Servicos');
    });

    session()->flash('success', 'Solicitação encaminhada com sucesso.');

    return back();
});
