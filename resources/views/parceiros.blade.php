@extends('layout')

@section('content')
    <div id="content" style="margin-top: -70px;">

        <!-- page-banner-section
            ================================================== -->
        <section class="page-banner-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2>Parceiros</h2>
                    </div>
                    <div class="col-md-6">
                        <ul class="page-pagin">
                            <li><a href="/">Início</a></li>
                            <li><a href="/parceiros">Parceiros</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- End page-banner section -->

        <!-- services-section
        Porto Seguro, Azul Seguros, Itau Seguros, Amil,
         Allianz, Bradesco Seguros, Chubb Seguros, HDI,
         Liberty, Yasuda Maritima, Mapfre, NotreDame,
          Tokio Marine, Sul América, Zurich,
            ================================================== -->
        <section id="services-section2">
            <div class="services-box-line">
                <div class="col-lg-3 center">
                    <a target="_blank" href="http://www.portoseguro.com.br/">
                        <img height="140" width="190" src="images/seguradoras/porto.jpg" alt="Porto Seguro Iasa">
                    </a>
                </div>
                <div class="col-lg-3 center">
                    <a target="_blank" href="https://www.azulseguros.com.br/">
                        <img height="140" width="190" src="images/seguradoras/azul.png" alt="Porto Seguro Iasa">
                    </a>
                </div>
                <div class="col-lg-3 center">
                    <a target="_blank" href="https://www.itau.com.br/proteja/?gclid=Cj0KEQjwvdSvBRDahavi3KPGrvUBEiQATZ9v0AT-PiYoxzKS19tXmeHnvp-bKJvYSB49VnumfMhIN_saAjeu8P8HAQ">
                        <img height="140" width="190" src="images/seguradoras/itau.png" alt="Porto Seguro Iasa">
                    </a>
                </div>
                <div class="col-lg-3 center">
                    <a target="_blank" href="http://www.amil.com.br/portal/web/institucional">
                        <img height="140" width="190" src="images/seguradoras/amil.png" alt="Porto Seguro Iasa">
                    </a>
                </div>
            </div>
            <div class="services-box-line">
                <div class="col-lg-3 center">
                    <a target="_blank" href="https://www.allianz.com.br/">
                        <img height="140" width="190" src="images/seguradoras/allianz.png" alt="Porto Seguro Iasa">
                    </a>
                </div>
                <div class="col-lg-3 center">
                    <a target="_blank" href="http://www.bradescoseguros.com.br/">
                        <img height="140" width="190" src="images/seguradoras/bradesco.png" alt="Porto Seguro Iasa">
                    </a>
                </div>
                <div class="col-lg-3 center">
                    <a target="_blank" href="http://www.chubb.com/">
                        <img height="140" width="190" src="images/seguradoras/chubb.png" alt="Porto Seguro Iasa">
                    </a>
                </div>
                <div class="col-lg-3 center">
                    <a target="_blank" href="https://www.hdi.com.br/">
                        <img height="140" width="190" src="images/seguradoras/hdi.png" alt="Porto Seguro Iasa">
                    </a>
                </div>

            </div>
            <div class="services-box-line">
                <div class="col-lg-3 center">
                    <a target="_blank" href="http://www.libertyseguros.com.br/institucional/index.aspx">
                        <img height="140" width="190" src="images/seguradoras/liberty.png" alt="Porto Seguro Iasa">
                    </a>
                </div>
                <div class="col-lg-3 center">
                    <a target="_blank" href="http://www.yasudamaritima.com.br/">
                        <img height="140" width="190" src="images/seguradoras/yasuda.png" alt="Porto Seguro Iasa">
                    </a>
                </div>
                <div class="col-lg-3 center">
                    <a target="_blank" href="http://www2.mapfre.com.br/home">
                        <img height="140" width="190" src="images/seguradoras/mapfre.png" alt="Porto Seguro Iasa">
                    </a>
                </div>
                <div class="col-lg-3 center">
                    <a target="_blank" href="http://www.notredame.com.br/web/portal/home">
                        <img height="140" width="190" src="images/seguradoras/notredame.png" alt="Porto Seguro Iasa">
                    </a>
                </div>
            </div>
            <div class="services-box-line">
                <div class="col-lg-3 center">
                    <a target="_blank" href="http://www.tokiomarine.com.br/">
                        <img height="140" width="190" src="images/seguradoras/tokiomarine.png" alt="Porto Seguro Iasa">
                    </a>
                </div>
                <div class="col-lg-3 center">
                    <a target="_blank" href="http://portal.sulamericaseguros.com.br/home.htm">
                        <img height="140" width="190" src="images/seguradoras/sulamerica.png" alt="Porto Seguro Iasa">
                    </a>
                </div>
                <div class="col-lg-3 center">
                    <a target="_blank" href="http://www.zurichseguros.com.br/">
                        <img height="140" width="190" src="images/seguradoras/zurich.png" alt="Porto Seguro Iasa">
                    </a>
                </div>
            </div>
        </section>


        <div class="modal fade" id="modal-id">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Solicitar cotação</h4>
                    </div>
                    <div class="modal-body">
                        <form id="formCotacao" action="enviarSolicitacao.php" method="post">
                            <div class="form-group">
                                <label for="nome">Nome</label>
                                <input class="form-control" type="text" name="nome" id="nome">
                            </div>

                            <input type="hidden" name="tipo" value="" id="tipoCotacao">

                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input class="form-control" type="text" name="email" id="email">
                            </div>

                            <div class="form-group">
                                <label for="telefone">Telefone</label>
                                <input class="form-control" type="text" name="telefone" id="telefone">
                            </div>

                            <div class="form-group">
                                <label for="encontrou">Como nos encontrou?</label>
                                <input class="form-control" type="text" name="encontrou" id="encontrou">
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="btnEnviarSolicitacao" type="button" class="btn btn-primary">Enviar Solicitação
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End services section -->

    </div>
@endsection