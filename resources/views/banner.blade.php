<li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on"
    data-title="{{ $title }}">
    <img height="1000" src="/images/banners/{{$number}}.jpg" alt="slidebg1" data-lazyload="/images/banners/{{$number}}.jpg"
    data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
    <div class="tp-caption finewide_medium_white lft tp-resizeme rs-parallaxlevel-0"
        data-x="{{ $x }}"
        data-y="{{ $y }}"
        data-speed="10"
        data-start="100"
        data-splitin="none"
        data-splitout="none"
        data-elementdelay="0.1"
        data-endelementdelay="0.1"
        style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;font-size: 40px;color: #FFF;text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;">
        <span style="font-family: Corben;"></span>
    </div>
</li>
