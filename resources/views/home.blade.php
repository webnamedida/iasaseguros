@extends('layout')
@section('styles')
@parent
<link rel="stylesheet" href="{{ asset('css/slick/slick/slick.css') }}">
<link rel="stylesheet" href="{{ asset('css/slick/slick/slick-theme.css') }}">
<style>
    .valor {color: #555;}
    .valor p {margin-bottom: 4px;color: #555;}
    a.vida .active {background: red;}
    .title-section * {font-family: 'Corben';font-size: 110%;line-height: 30px;}
    #valores {background-image: url("images/confianca.png");}
    #valores h4, #valores p {color: #FFF;}
</style>
@endsection
@section('scripts')
@parent
<script src="{{ asset('css/slick/slick/slick.js') }}"></script>
<script>
$('.about-post-content a').click(function  () {
	window.open($(this).prop('href'));
});
if (location.href.indexOf('sobre') != -1) {
    $('body').scrollTo(505, {
        duration: 'slow',
        offsetTop: '50'
    });
}
$('#sobre').click(function  () {
    $('body').scrollTo(505, {duration: 'slow', offsetTop: '50'});
});
$('#valores').slick({
    arrows: false,
    dots: true,
    infinite: true,
    speed: 1700,
    autoplay: true,
    autoplaySpeed: 15000,
    centerPadding: '0',
});
if($(window).width() <= 768) {
    $('.tp-banner').hide();
}
else {
    $('.tp-banner').show();
}
</script>
@endsection
@section('content')
<section id="home-section" class="slider1 container">
    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>
                @include('banner', ['title' => 'ESTRUTURA', 'number' => 1, 'x' => 200, 'y' => 120])
                @include('banner', ['title' => 'RESPONSABILIDADE', 'number' => 2, 'x' => 200, 'y' => 120])
                @include('banner', ['title' => 'ATENDIMENTO', 'number' => 3, 'x' => 200, 'y' => 120])
                @include('banner', ['title' => 'CONFORTO', 'number' => 4, 'x' => 200, 'y' => 120])
                @include('banner', ['title' => 'CONFIANÇA', 'number' => 5, 'x' => 200, 'y' => 120])
                @include('banner', ['title' => 'SEGURANÇA', 'number' => 6, 'x' => 200, 'y' => 120])
            </ul>
            <div class="tp-bannertimer"></div>
        </div>
    </div>
</section>
<section id="about-section">
    <div class="container">
        <div class="title-section">
            <h1><strong>Conheça a</strong> IASA SEGUROS</h1>
            <p style="margin-bottom: 20px;color: #555"><strong>Histórico da Empresa</strong> - Fundada em outubro de 1990, a Iasa Corretora de Seguros teve
            desde seu início como diferencial o atendimento
            próximo ao cliente, que visava de modo transparente a melhor escolha para todas as partes. Do começo
            de nossa caminhada, com apenas uma sala comercial em Santo André e uma equipe muito pequena, até
            hoje, com nossa sede própria e equipe definida, nosso crescimento deu se pelo boca a boca e
            satisfação dos clientes.
            Agora, convidamos você a conhecer-nos, nessa reinvenção de nossa empresa. Visite nosso prédio, faça
            uma cotação. <br><br>
            <strong>"Cliente Iasa é cliente satisfeito."</strong>
            </p>
            <div id="valores" class="title-section well">
                <div class="valor">
                    <h4>Missão</h4>
                    <p>Garantir ao cliente uma consultoria exclusiva (particular), profissional e
                    satisfatória visando alcançar fidelidade e retorno comercial, sempre trabalhando com
                    responsabilidade judicial, social e ambiental.</p>
                </div>
                <div class="valor">
                    <h4>Visão</h4>
                    <p>Alcançar o reconhecimento, satisfação e recomendação de nossos clientes,
                    tornando-nos referencia em seguros, qualidade
                    de serviços e rentabilidade no ABC paulista.</p>
                </div>
                <div class="valor">
                    <h4>Valores</h4>
                    <p>Na Iasa Seguros, nossos valores dizem como nosso comportamento deve ser,
                    nos guiamos pelos seguintes princípios:
                    Seguro Se vende com Honestidade.
                    O respeito é um dever e direito de todo ser humano.
                    Sozinhos somos limitados, mas com Companheirismo somos invencíveis
                    Tradição se conquista com Integridade e Eficiência
                    O sucesso é uma consequência da busca pela excelência.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="about-box">
        <div class="inner-about-box">
            <div class="about-post">
                <div class="about-post-content">
                    <a class="saude" target="_blank" href="http://www.portoseguro.com.br/seguros/saude-e-odonto/seguradora/saude"><i class="fa fa-medkit"></i></a>
                    <h2>saúde</h2>
                    <p>Empresarial, pequenas e médias empresas.</p>
                </div>
                <div class="hover-about">
                    <h2>saúde</h2>
                    <h1 style="font-size: 23px;">O equilibrio entre a qualidade de vida dos seus funcionários e os investimentos de sua empresa</h1>
                    <p>
                    O seguro que oferece médicos de qualidade, ampla rede referenciada, coberturas abrangentes e uma
                    série de
                    benefícios e vantagens para seus colaboradores, além de diversas opções de planos, acesso rápido às
                    informações
                    e atendimento pós-venda exclusivo para o RH da sua empresa.
                    </p>
                </div>
            </div>
            <div class="about-post">
                <div class="about-post-content">
                    <a class="empresa" target="_blank" href="http://www.portoseguro.com.br/seguros/seguro-para-seus-negocios/seguros-empresariais"><i class="fa fa-building-o"></i></a>
                    <h2>empresa</h2>
                    <p>Sua empresa protegida, não importa o tamanho de suas necessidades.</p>
                </div>
                <div class="hover-about">
                    <h2>empresa</h2>
                    <h1 style="font-size: 23px;">Serviços para sua empresa</h1>
                    <p>Existem muitas maneiras de proteger a sua empresa, mas só uma vai garantir o seu patrimônio com
                    eficiência, segurança e economia: Porto Seguro Empresa. Um seguro em que você contrata a Cobertura
                    Básica e diversas Coberturas Opcionais que oferecem a proteção ideal para pequenas, médias e grandes
                    empresas.</p>
                </div>
            </div>
            <div class="about-post">
                <div class="about-post-content">
                    <a class="vida" target="_blank" href="http://www.portoseguro.com.br/seguros/seguro-de-vida/seguro-de-vida-para-voce/linha-de-produtos/seguro-de-vida-mais-mulher"><i class="fa fa-female"></i></a>
                    <h2>Vida mais mulher</h2>
                    <p>Um motivo a mais para você se sentir protegida.</p>
                </div>
                <div class="hover-about">
                    <h2>Vida mais mulher</h2>
                    <h1 style="font-size: 23px;">Vantagens para você, mulher</h1>
                    <p>
                    Enquanto você cuida da sua família, da casa e dos negócios, o Porto Seguro Vida Mais Mulher cuida de
                    você.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection