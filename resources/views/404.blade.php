<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Iasa Seguros - Página não encontrada</title>
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    </head>
    <body>
        <div class="container">
            <div class="jumbotron text-center">

            	<div class="center">
            		<img src="{{ asset('images/logo.png') }}" height="309" width="400" alt="Iasa Seguros">
            	</div>
                <h1>404</h1>
                <h2>Página não encontrada</h2>, 
                <h3>Clique <a href="/">aqui</a> para voltar a Home</h3>
            </div>
        </div>
    </body>
</html>