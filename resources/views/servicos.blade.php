@extends('layout')

@section('scripts')
    @parent
    <script>
        $('#btnEnviarSolicitacao').click(function  () {
            $('form').submit();
        });
    </script>
@endsection

@section('content')
<div id="content" style="margin-top: -70px;">
    <!-- page-banner-section
    ================================================== -->
    <section class="page-banner-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Nossos Serviços</h2>
                </div>
                <div class="col-md-6">
                    <ul class="page-pagin">
                        <li><a href="/home">Início</a></li>
                        <li><a href="/servicos">Nossos Serviços</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- End page-banner section -->
    <!-- services-section
    ================================================== -->
    <section id="services-section2">
        <div class="container">
            @if(session()->get('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @endif
            <div class="title-section">
                <h1>Conheça nossos ramos de trabalho</span></h1>
                <span></span>
                <p>Mais do que tudo, nossa filosofia é não medir esforcos para atingir a Excelencia no atendimento.
                Nossa equipe está sempre a sua disposicão para orientar o seguro mais indicado dentro do seu
                perfil, esclarecendo dúvidas, sanando divergencias. Entendemos que além de um relacionamento
                comercial temos que vivenciar as necessidades de nossos amigos e clientes principalmente em
                ocasiões de sinistros e prontidão nas informacões.</p>
            </div>
        </div>
        <div class="services-box-line">
            <div class="services-post">
                <a href="javascript:;"><i class="fa fa-car"></i></a>
                <h2>Auto</h2>
                <h5>Seu principal meio de transporte garantido.</h5>
                <p><a class="solicitar_cotacao btn btn-primary btn-sm" data-toggle="modal" href='#modal-id' href="javascript:;">Solicite uma cotação</a></p>
            </div>
            <div class="services-post">
                <a href="javascript:;"><i class="fa fa-laptop"></i></a>
                <h2>Equipamentos Portáteis</h2>
                <h5>Assistência para equipamentos eletrônicos.</h5>
                <p><a class="solicitar_cotacao btn btn-primary btn-sm" data-toggle="modal" href='#modal-id' href="javascript:;">Solicite uma cotação</a></p>
            </div>
            <div class="services-post">
                <a href="javascript:;"><i class="fa fa-building-o"></i></a>
                <h2>Empresa</h2>
                <h5>Funcionamento da sua empresa conservado.</h5>
                <p><a class="solicitar_cotacao btn btn-primary btn-sm" data-toggle="modal" href='#modal-id' href="javascript:;">Solicite uma cotação</a></p>
            </div>
        </div>
        <div class="services-box-line">
            <div class="services-post">
                <a href="javascript:;"><i class="fa fa-paw"></i></a>
                <h2>Health for Pet</h2>
                <h5>Plano de saúde para seu melhor amigo.</h5>
                <p><a class="solicitar_cotacao btn btn-primary btn-sm" data-toggle="modal" href='#modal-id' href="javascript:;">Solicite uma cotação</a></p>
            </div>
            <div class="services-post">
                <a href="javascript:;"><i class="fa fa-dollar"></i></a>
                <h2>Fiança Locatícia</h2>
                <h5>Aluguel com mais tranqüilidade e segurança.</h5>
                <p><a class="solicitar_cotacao btn btn-primary btn-sm" data-toggle="modal" href='#modal-id'
                href="javascript:;">Solicite uma cotação</a></p>
            </div>
            <div class="services-post">
                <a href="javascript:;"><i class="fa fa-users"></i></a>
                <h2>Vida em grupo e Individual</h2>
                <h5>Estabilidade da sua família ou empresa garantida.</h5>
                <p><a class="solicitar_cotacao btn btn-primary btn-sm" data-toggle="modal" href='#modal-id'
                href="javascript:;">Solicite uma cotação</a></p>
            </div>
        </div>
        <div class="services-box-line">
            <div class="services-post">
                <a href="javascript:;"><i class="fa fa-user-md"></i></a>
                <h2>Plano Odontológico</h2>
                <h5>Sua saúde dental assegurada.</h5>
                <p><a class="solicitar_cotacao btn btn-primary btn-sm" data-toggle="modal" href='#modal-id'
                href="javascript:;">Solicite uma cotação</a></p>
            </div>
            <div class="services-post">
                <a href="javascript:;"><i class="fa fa-medkit"></i></a>
                <h2>Plano de Saúde</h2>
                <h5>Os melhores hospitais a sua disposição.</h5>
                <p><a class="solicitar_cotacao btn btn-primary btn-sm" data-toggle="modal" href='#modal-id'
                href="javascript:;">Solicite uma cotação</a></p>
            </div>
            <div class="services-post">
                <a href="javascript:;"><i class="fa fa-building"></i></a>
                <h2>Riscos de engenharia</h2>
                <h5>Construções protegidas de todos os riscos.</h5>
                <p><a class="solicitar_cotacao btn btn-primary btn-sm" data-toggle="modal" href='#modal-id'
                href="javascript:;">Solicite uma cotação</a></p>
            </div>
        </div>
        <div class="services-box-line">
            <div class="services-post">
                <a href="javascript:;"><i class="fa fa-bus"></i></a>
                <h2>Transporte</h2>
                <h5>Sua carga preservada.</h5>
                <p><a class="solicitar_cotacao btn btn-primary btn-sm" data-toggle="modal" href='#modal-id'
                href="javascript:;">Solicite uma cotação</a></p>
            </div>
            <div class="services-post">
                <a href="javascript:;"><i class="fa fa-plane"></i></a>
                <h2>Viagem</h2>
                <h5>Curta suas férias despreocupado.</h5>
                <p><a class="solicitar_cotacao btn btn-primary btn-sm" data-toggle="modal" href='#modal-id'
                href="javascript:;">Solicite uma cotação</a></p>
            </div>
            
            {{-- <div class="services-post">
                <a href="javascript:;"><i class="fa fa-bank"></i></a>
                <h2>Licitações</h2>
                <h5>Texto sobre licitacoes</h5>
                <p><a class="solicitar_cotacao btn btn-primary btn-sm" data-toggle="modal" href='#modal-id'
                href="javascript:;">Solicite uma cotação</a></p>
            </div> --}}
        </div>
    </section>
    <div class="modal fade" id="modal-id">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Solicitar cotação</h4>
                </div>
                <div class="modal-body">
                    <form id="formCotacao" action="/servicos" method="post">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input class="form-control" type="text" name="nome" id="nome" required>
                        </div>
                        <input type="hidden" name="tipo" value="" id="tipoCotacao">
                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input class="form-control" type="text" name="email" id="email" required>
                        </div>
                        <div class="form-group">
                            <label for="telefone">Telefone</label>
                            <input class="form-control" type="text" name="telefone" id="telefone" required>
                        </div>
                        <div class="form-group">
                            <label for="encontrou">Ramo de interesse</label>
                            <input class="form-control" type="text" name="interesse" id="interesse" required>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button id="btnEnviarSolicitacao" type="button" class="btn btn-primary">Enviar Solicitação</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End services section -->
</div>
@endsection