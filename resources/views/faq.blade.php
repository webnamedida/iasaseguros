@extends('layout')
@section('styles')
@parent
<link href='https://fonts.googleapis.com/css?family=Concert+One' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Comfortaa' rel='stylesheet' type='text/css'>
<style>
    h3 {
        text-align: center;
        background: #aac7e2;
        font-family: 'gill sans';
        font-weight: bold;
    }
    .pergunta {
        padding: 12px;
        font-family: 'Lato';
        font-size: 15px;
        background-color: #3b8e68;
        margin-bottom: 3px;
        color: white;
        cursor: pointer;
    }    
    .resposta {
        display: none;
    }
    .modal-body, .modal-title {
        font-family: 'Lato';
    }
</style>
@stop
@section('scripts')
@parent
<script>
    $('.pergunta').on('click', function  () {
        $('#modal_title').text($(this).text());
        $('#modal_content').text($(this).next('.resposta').text());
    });
</script>
@stop
@section('content')
<div id="content" style="margin-top: -70px;">
    <section class="page-banner-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>FAQ</h2>
                </div>
                <div class="col-md-6">
                    <ul class="page-pagin">
                        <li><a href="/">Início</a></li>
                        <li><a href="/faq">FAQ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- End page-banner section -->
    <!-- services-section
    ================================================== -->
    <section id="services-section2">
        <div class="container">
            <div class="title-section">
                <h1>Perguntas Frequentes</span></h1>
                <span></span>
            </div>
            <div class="modal fade" id="modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modal_title"></h4>
                        </div>
                        <div class="modal-body" id="modal_content"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="col-lg-10 col-lg-offset-1">
                    <h3 class="well" style="background: #aac7e2; border: 1px solid #aac7e2">AUTOMÓVEL</h3>
                    <div class="pergunta" data-toggle="modal" href="#modal">Após o termino da vigência ainda tenho extensão de cobertura?</div>
                    <div class="resposta">Na maioria das companhias a cobertura termina junto com o fim da vigência. Algumas companhias, entretanto, oferecem extensão de cobertura por mais 05 dias após o termino de vigência como é o caso da Porto, Itaú e Azul.</div>
                    <div class="pergunta" data-toggle="modal" href="#modal">Nunca bati meu veiculo ou me roubaram, por que sinto que o valor está alto?</div>
                    <div class="resposta">
                        Todo ano, assim como em outros ramos de prestação de serviços, os seguros sofrem
                        reajuste influenciado pelo aumento dos registros de roubo, furto, colisão e também com
                        base nas entradas e perdas de cada companhia alem da influencia da inflação.<br>Então se o
                        seu seguro aumentou, não é um caso isolado.
                    </div>
                    <div class="pergunta" data-toggle="modal" href="#modal">Qualquer pessoa pode dirigir meu veiculo?</div>
                    <div class="resposta">
                        O principal condutor indicado no perfil deve dirigir o veiculo por 75% do tempo, quando cumprida essa exigência, outros condutores podem sim dirigir o veiculo ocasionalmente.<br>
                        Se por um acaso um desses condutores tiver menos de 25 anos, essa informação deve ser
                        citada na apólice.
                    </div>
                    <div class="pergunta" data-toggle="modal" href="#modal">
                        Como funciona minha classe de bônus para seguro?
                    </div>
                    <div class="resposta">
                        A cada ano que o seguro for renovado sem ter sido de fato ativado por sinistro, o segurado
                        ganha uma classe de bônus que aumenta gradativamente até a classe limite que é a 10.
                        <br>Essas classes de bônus fazem com que o seguro não sofra um aumento significativo na
                        renovação. A cada vez que o seguro for ativado, o cliente perde uma classe de bônus na
                        vigência.
                    </div>
                    <div class="pergunta" data-toggle="modal" href="#modal">Existe diferença entre fazer seguro em um banco e uma corretora de seguros?</div>
                    <div class="resposta">
                        Na maioria dos casos, pela nossa experiência de clientes que vieram de bancos, o grande
                        diferencial é o atendimento, seja ele em caso de sinistro, ou alterações na apólice, na Iasa
                        corretora, você sempre encontrará um profissional disposto a prestar uma consultoria de
                        qualidade visando sempre as melhores alternativas.
                    </div>
                </div>
                <div class="col-lg-10 col-lg-offset-1">
                    <h3 class="well" style="background: #aac7e2; border: 1px solid #aac7e2">SINISTRO</h3>
                    <div class="pergunta" data-toggle="modal" href="#modal">Como proceder em caso de sinistro? E se eu for terceiro?</div>
                    <div class="resposta">
                        Em caso de sinistro, de qualquer natureza que seja, o primeiro passo é realizar o boletim
                        de ocorrências, que inclusive pode ser feito online pelo link -
                        <a target="_blank" href="http://www.ssp.sp.gov.br/nbo/">http://www.ssp.sp.gov.br/nbo/</a><br>
                        Depois, após obter os contatos do terceiro aconselhamos ligar a seu corretor para auxiliá-lo na abertura do processo de sinistro junto a companhia.
                        Se você for o terceiro, após a abertura do boletim de ocorrência e com os contatos do
                        primeiro, procure também seu corretor para confirmar o procedimento.
                    </div>
                    <div class="pergunta" data-toggle="modal" href="#modal">O que são danos materiais e corporais?</div>
                    <div class="resposta">
                        São as coberturas que cobrem danos materiais ou corporais involuntários que possam ser
                        causados a terceiros pelo veiculo segurado, de acordo com o limite da apólice, ele
                        reembolsa as indenizações que o segurado tenha que pagar, de modo judicial ou
                        extrajudicial. Nesse valor também está incluso o pagamento de advogado e custos
                        judiciais.
                    </div>
                    <div class="pergunta" data-toggle="modal" href="#modal">O que é franquia e como funciona?</div>
                    <div class="resposta">Franquia é a taxa paga pelo segurado para ativação do seguro, seja ela decorrente de
                        roubo, furto, colisão ou incêndio, o pagamento se faz necessário para que o segurado
                        receba a indenização ou concerto do veiculo. <br>Por isso é sempre uma questão importante a
                    ser confirmada durante a contratação.</div>
                    <div class="pergunta" data-toggle="modal" href="#modal">Meu sinistro pode ser recusado?</div>
                    <div class="resposta">
                        Sim. De acordo com as clausulas de toda contratação, as companhias se reservam o direito de recusar o sinistro em alguns casos, os mais recorrentes são de suspeita ou confirmação de fraude, embriaguez comprovada, documentação do veiculo/condutor vencidas ou suspendidas, vandalismo.
                    </div>
                    <div class="pergunta" data-toggle="modal" href="#modal">Uma pessoa sem seguro bateu no meu veiculo, podemos fazer um acordo em que ela pague a minha franquia?</div>
                    <div class="resposta">
                        Não. Ao ativar o seguro, entende-se que foi devido a um acidente causado pelo segurado.
                        Ao fazer com que a seguradora pague por um acidente que foi causado por um terceiro, que não tem uma apólice para cobrir o dano, o causador do acidente sai no lucro, já que não terá que pagar o valor total do concerto e deixará assim, a seguradora no prejuízo.
                        Por isso, esse tipo de acordo é classificado como fraude para as companhias.
                    </div>
                </div>
                <div class="col-lg-10 col-lg-offset-1">
                    <h3 class="well" style="background: #aac7e2; border: 1px solid #aac7e2">VIDA E PREVIDÊNCIA</h3>
                    <div class="pergunta" data-toggle="modal" href="#modal">O seguro de vida pode ser feito para todas pessoas?</div>
                    <div class="resposta">
                        Até 64 anos na teoria dos podem contratar o seguro. <br>Porém, fumantes e pessoas que
                        tenham predisposição a alguma doença ou que já estejam em processo de tratamento de
                        alguma doença terminal tem dificuldade em ter o risco aceito pelas companhias. <br>Por isso,
                        o indicado é sempre iniciar o seguro quando se tem uma boa qualidade de vida, e não
                        esperar algo acontecer para realizar a contratação.
                    </div>
                    <div class="pergunta" data-toggle="modal" href="#modal">Quais são as coberturas de seguro de vida e sua importância?</div>
                    <div class="resposta">As principais coberturas são de morte – Natural ou Acidental e de Invalidez Permanente,
                        <br>Em ambos os casos, a principal importância do seguro de vida é garantir que a família
                        possa passar por tais momentos com o auxilio financeiro necessário para se colocar
                    novamente nos trilhos.</div>
                    <div class="pergunta" data-toggle="modal" href="#modal">O que é previdência privada?</div>
                    <div class="resposta">
                        É uma forma de seguro que visa condicionar ao segurado uma manutenção do seu padrão
                        de vida em sua aposentadoria.<br> Além de sua renda vinda de contribuição social, a
                        previdência privada garante ao segurado uma renda mensal que fará com que ele possa
                        desfrutar da melhor maneira possível a melhor etapa da vida.
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection