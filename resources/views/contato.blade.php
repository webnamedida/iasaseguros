@extends('layout')
@section('scripts')
@parent
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="js/gmap3.min.js"></script>
@endsection
@section('content')
<section class="page-banner-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>Contato</h2>
            </div>
            <div class="col-md-6">
                <ul class="page-pagin">
                    <li><a href="/">Início</a></li>
                    <li><a href="/contato">Contato</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id="contato-section">
    <div class="contato-info">
        <div class="container">
            @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif
            @if($errors->has())
            {{ $errors->first()}}
            @endif
            <div class="title-section">
                <h1>Entre em contato</h1>
                <span></span>
            </div>
            <div class="contato-info-box">
                <div class="row">
                    <div class="col-md-3">
                        <span><i class="fa fa-map-marker"></i></span>
                        <h2>Localização</h2>
                        <p>Avenida Araucária, 1232 <br> Santo André, São Paulo 09251-040, Brasil</p>
                    </div>
                    <div class="col-md-3">
                        <span><i class="fa fa-phone"></i></span>
                        <h2>Telefones</h2>
                        <p> Tel.: (11) 4476-1255 <br> Tel.: (11) 94782-8879</p>
                    </div>
                    <div class="col-md-3">
                        <span><i class="fa fa-envelope"></i></span>
                        <h2>E-mail</h2>
                        <p> iasaseguros@iasaseguros.com.br</p>
                    </div>
                    <div class="col-md-3">
                        <span><i class="fa fa-facebook"></i></span>
                        <h2>Facebook</h2>
                        <p>
                            <a target="_blank" href="https://www.facebook.com/pages/Iasa-Corretora-de-Seguros/569648983050735">
                                Iasa Seguros
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="map"></div>
    <div class="contato-form">
        <div class="container">
            <form id="contato-form" method="post">
                {!! csrf_field() !!}
                <h2>ENVIE NOS UMA MENSAGEM</h2>
                <div class="row">
                    <div class="col-md-4">
                        <input name="nome" id="name" type="text" required="required" placeholder="Digite seu Nome">
                    </div>
                    <div class="col-md-4">
                        <input name="email" id="mail" type="text" required="required" placeholder="Digite seu Email">
                    </div>
                    <div class="col-md-4">
                        <input name="assunto" id="website" type="text" required="required" placeholder="Digite o Assunto">
                    </div>
                </div>
                <textarea name="mensagem" id="comment" placeholder="Digite sua mensagem" required="required"></textarea>
                <div class="submit-area">
                    <input type="submit" value="Enviar Mensagem">
                    <div id="msg" class="message"></div>
                </div>
            </form>
            
        </div>
    </div>
</section>
@endsection