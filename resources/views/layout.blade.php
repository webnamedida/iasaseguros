<!doctype html>
<html lang="en" class="no-js">
    <head>
        <title>Iasa Seguros</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        @section('styles')
            <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
            <link href='https://fonts.googleapis.com/css?family=Corben' rel='stylesheet' type='text/css'>
            <link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}">
            <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}" media="screen">
            <link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}" media="screen">
            <link rel="stylesheet" type="text/css" href="{{ asset('css/settings.css') }}" media="screen"/>
            <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" media="screen">
            @show
        </head>
        <body>
            <div id="container">
                <header class="clearfix" style="margin-bottom: 10px;">
                    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                        <nav class="navbar" style="min-height: 20px;border-bottom: 1px solid #EEE">
                            <div class="pull-right">
                                <a style="margin-left: 22px;" target="_blank" href="https://www.facebook.com/pages/Iasa-Corretora-de-Seguros/569648983050735">
                                 <i class="fa fa-facebook-official"></i>
                                </a>
                                <a style="margin-left: 22px;" href="javascript:;"><i class="fa fa-phone"> (11) 4476-1255</i></a>
                                <a href="javascript:;"><i class="fa fa-phone"> (11) 94782-8879</i></a>
                            </div>
                        </nav>
                        <div class="container">
                            <div class="navbar-header pull-right">
                                <a class="navbar-brand" href="/" style="padding:0; margin:0">
                                <img src="images/logo/logo.png" width="100" alt="Iasa seguros">
                                </a>
                            </div>
                            <div class=" navbar-collapse pull-left" style="margin-top: 30px;">
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="megadrop"><a class="active" href="/">Home</a></li>
                                    <li class="drop"><a href="/#sobre" id="sobre">Sobre</a></li>
                                    <li class="drop"><a href="/servicos">Serviços</a></li>
                                    <li class="drop"><a href="/faq">Faq</a></li>
                                    <li class="drop"><a href="/parceiros">Parceiros</a></li>
                                    <li><a href="/contato">Contato</a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </header>
                <div id="content" style="margin-top: 72px;">
                    @yield('content')
                </div>
                <footer style="margin-top: 30px;">
                    <div class="container">
                        <p class="copyright">
                        &copy; Copyright 2015 IASA SEGUROS - Todos os direitos reservados.
                        - Desenvolvido por <a href="http://informaticaexpress.com.br">Informática Express</a>
                        </p>
                    </div>
                </footer>
            </div>
            @section('scripts')
                <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
                <script type="text/javascript" src="{{ asset('js/jquery.migrate.js') }}"></script>
                <script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>
                <script type="text/javascript" src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
                <script type="text/javascript" src="{{ asset('js/jquery.bxslider.min.js') }}"></script>
                <script type="text/javascript" src="{{ asset('js/jquery.appear.js') }}"></script>
                <script type="text/javascript" src="{{ asset('js/jquery.countTo.js') }}"></script>
                <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
                <script type="text/javascript" src="{{ asset('js/jquery.imagesloaded.min.js') }}"></script>
                <script type="text/javascript" src="{{ asset('js/jquery.isotope.min.js') }}"></script>
                <script type="text/javascript" src="{{ asset('js/retina-1.1.0.min.js') }}"></script>
                <script type="text/javascript" src="{{ asset('js/plugins-scroll.js') }}"></script>
                <script type="text/javascript" src="{{ asset('js/smooth-scroll.js') }}"></script>
                <script type="text/javascript" src="{{ asset('js/waypoint.min.js') }}"></script>
                <script type="text/javascript" src="{{ asset('js/raphael-min.js') }}"></script>
                <script type="text/javascript" src="{{ asset('js/DevSolutionSkill.min.js') }}"></script>
                <script type="text/javascript" src="{{ asset('js/jquery.themepunch.tools.min.js') }}"></script>
                <script type="text/javascript" src="{{ asset('js/jquery.themepunch.revolution.min.js') }}"></script>
                <script type="text/javascript" src="{{ asset('js/script.js') }}"></script>
                <script src="{{ asset('js/site/layout.js') }}"></script>
            @show
        </body>
    </html>